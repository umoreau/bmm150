BMM150 API
==========

.. autoenum:: bmm150.bmm150_defs.PowerMode
   :members:

.. autoenum:: bmm150.bmm150_defs.PresetMode
   :members:

.. autoenum:: bmm150.bmm150_defs.PowerControl
   :members:

.. autoclass:: bmm150.BMM150
   :members:
