Introduction
============

The BMM150 is a low-power, low-noise BMM150 is a 3-axis digital geomagnetic sensor produced by BOSH Sensortech's.
It provides he BMM150 provides absolute spatial orientation and motion vectors with high accuracy and dynamics.

This library is a pure python API interfacing this sensor through **i2c**.

Installation
------------

The easiest way to install this library is using pip:

```bash
pip install bmm150
```

Quickstart
----------

The following code initializes the sensor, and prints the magnetic field values for x, y and z.
Then, using the `atan2` function from the `math` standard library, it retrieves the heading of the sensor.

.. code-block:: python

    import bmm150
    import math

    device = bmm150.BMM150()

    x, y, z = device.read_mag_data()

    heading_rads = math.atan2(x, y)

    heading_degrees = math.degrees(heading_rads)

    print(f"X : {x:.2f}µT")
    print(f"Y : {y:.2f}µT")
    print(f"Z : {z:.2f}µT")

    print(f"Heading: {heading_degs:.2f}°")

