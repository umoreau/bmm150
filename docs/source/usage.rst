Usage
=====

Sample code
-----------

The following code initializes the sensor, and prints the magnetic field values for x, y and z.
Then, using the `atan2` function from the `math` standard library, it retrieves the heading of the sensor.

.. code-block:: python

    import bmm150
    import math

    device = bmm150.BMM150()

    x, y, z = device.read_mag_data()

    heading_rads = math.atan2(x, y)

    heading_degrees = math.degrees(heading_rads)

    print(f"X : {x:.2f}µT")
    print(f"Y : {y:.2f}µT")
    print(f"Z : {z:.2f}µT")

    print(f"Heading: {heading_degs:.2f}°")

Presets
-------

The bmm150 sensor provides 4 different presets, offering a trade between accuracy and power consumption.
The more accuracy is required, the more measures will be made, thus the more the power consumption will raise.

On each preset, *n* measures will be taken and averaged out, noted by the number of repetitions (*Rep X/Y* for the X and Y axis, *Rep Z* for the Z axis).
The Recommended ODR (Output Data Rate) indicates the rate at which the data might be polled for optimal results.

The presets are detailed in the following table:

+--------------------------+-----------+---------+------------------+
| Preset                   | Rep. X/Y  | Rep. Z  | Recommended ODR  |
+==========================+===========+=========+==================+
| Low power preset         | 3         | 3       | 10               |
+--------------------------+-----------+---------+------------------+
| Regular preset           | 9         | 15      | 10               |
+--------------------------+-----------+---------+------------------+
| Enhanced regular preset  | 15        | 27      | 10               |
+--------------------------+-----------+---------+------------------+
| High accuracy preset     | 47        | 83      | 20               |
+--------------------------+-----------+---------+------------------+

A preset can be specified when instantiating a BMM150.

.. code-block:: python

    import bmm150

    device = bmm150.BMM150(presetmode = bmm150.PresetMode.LOWPOWER)
    device = bmm150.BMM150(presetmode = bmm150.PresetMode.REGULAR)
    device = bmm150.BMM150(presetmode = bmm150.PresetMode.HIGHACCURACY)
    device = bmm150.BMM150(presetmode = bmm150.PresetMode.ENHANCED)
