VERSION=$(shell poetry version -s)

.PHONY: help reformat check tests clean tox

help:
	@echo -e "bmm150 version $(VERSION) -- Makefile"
	@echo -e ""
	@echo -e "\t help\t\t cf. `make help`"
	@echo -e "\t flake8\t\t Check code style"
	@echo -e "\t mypy\t\t Check static typing"
	@echo -e "\t isort\t\t Check import order"
	@echo -e "\t tests\t\t Run unit tests"
	@echo -e "\t tox\t\t Run unit tests for different python versions"
	@echo -e "\t check\t\t Run a dependency vulnerability test"
	@echo -e "\t build\t\t Build as a wheel"
	@echo -e ""
	@echo -e "\t clean:\t\t Nettoyage complet du dossier de construction"
	@echo -e "\t reformat \t Reformate le code"

clean:
	rm -rf build/ dist/ .venv .mypy_cache .pytest_cache

poetry:
	which poetry || pip install poetry
	poetry install

flake8: poetry
	poetry install
	poetry run flake8 --ignore=E501,W503,F405,F403 tests
	poetry run flake8 --ignore=E501,W503,F405,F403 src

mypy: poetry
	poetry install
	poetry run mypy --ignore-missing  --check-untyped-defs src
	poetry run mypy --ignore-missing  --check-untyped-defs tests

isort: poetry
	poetry install
	poetry run isort --check src
	poetry run isort --check tests

reformat: poetry
	poetry install
	poetry run black .
	poetry run isort .

check: poetry
	poetry export --without-hashes -f requirements.txt | poetry run safety check --full-report --stdin

build: poetry
	poetry build

tox: poetry
	poetry run tox

tests: poetry
	poetry install
	poetry run pytest tests/ --cov=src --cov-report=html --junitxml=report.xml --html-report=./test_report --title='Bmm150 test report'
	poetry run coverage xml
	poetry run coverage report