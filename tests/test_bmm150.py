import sys

import pytest

from src.bmm150 import BMM150, ChipIdNotConformError

# Shamelessly stolen from https://github.com/kplindegaard/smbus2

##########################################################################
# Mock open, close and ioctl so we can run our unit tests anywhere.

# Required I2C constant definitions repeated
I2C_FUNCS = 0x0705  # Get the adapter functionality mask
I2C_SMBUS = 0x0720
I2C_SMBUS_WRITE = 0
I2C_SMBUS_READ = 1

I2C_SMBUS_QUICK = 0
I2C_SMBUS_BYTE_DATA = 2
I2C_SMBUS_WORD_DATA = 3
I2C_SMBUS_BLOCK_DATA = 5  # Can't get this one to work on my Raspberry Pi
I2C_SMBUS_I2C_BLOCK_DATA = 8
I2C_SMBUS_BLOCK_MAX = 32

MOCK_FD = "Mock BMM150"

test_buffer = [x for x in range(256)]


def bytes_six(lst):
    """convert a list of int to `bytes` like object"""
    if sys.version_info.major >= 3:
        return bytes(lst)
    else:
        return "".join(map(chr, lst))


def mock_open(*args):
    print("Mocking open: %s" % args[0])
    return MOCK_FD


def mock_close(*args):
    assert args[0] == MOCK_FD


def mock_read(fd, length):
    assert fd == MOCK_FD
    return bytes_six(test_buffer[0:length])


def mock_ioctl(fd, command, msg):
    print(f"\n(Mocking ioctl) Command :{hex(command)} | Message : {msg}")
    assert fd == MOCK_FD
    assert command is not None

    # Reproduce i2c capability of a Raspberry Pi 3 w/o PEC support
    if command == I2C_FUNCS:
        print("\t Command is I2C_FUNCS, returning 0xeff0001")
        msg.value = 0xEFF0001
        return

    # Reproduce ioctl read operations
    if command == I2C_SMBUS and msg.read_write == I2C_SMBUS_READ:
        print("\t Command is I2C_SMBUS:I2C_SMBUS_READ")

        offset = msg.command
        if msg.size == I2C_SMBUS_BYTE_DATA:
            print("\t\t Message size is I2C_SMBUS_BYTE_DATA")
            print(
                "\t\t Returning byte at offset {hex(offset)} => [{test_buffer[offset]}]"
            )
            msg.data.contents.byte = test_buffer[offset]
        elif msg.size == I2C_SMBUS_WORD_DATA:
            print("\t\t Message size is I2C_SMBUS_WORD_DATA")
            print(
                f"\t\t Returning word at offset {hex(offset)} => [{test_buffer[offset + 1] * 256 + test_buffer[offset]}]"
            )
            msg.data.contents.word = test_buffer[offset + 1] * 256 + test_buffer[offset]
        elif msg.size == I2C_SMBUS_I2C_BLOCK_DATA:
            print("\t\t Message size is I2C_SMBUS_I2C_BLOCK_DATA")
            for k in range(msg.data.contents.byte):
                msg.data.contents.block[k + 1] = test_buffer[offset + k]
            print(
                f"\t\t Returning block at offset {hex(offset)} => {[hex(test_buffer[offset + k]) for k in range(msg.data.contents.byte)]}"
            )

    if command == I2C_SMBUS and msg.read_write == I2C_SMBUS_WRITE:
        print("\t Command is I2C_SMBUS:I2C_SMBUS_WRITE")

        offset = msg.command
        if msg.size == I2C_SMBUS_BYTE_DATA:
            print("\t\t Message size is I2C_SMBUS_BYTE_DATA")
        elif msg.size == I2C_SMBUS_WORD_DATA:
            print("\t\t Message size is I2C_SMBUS_WORD_DATA")
        elif msg.size == I2C_SMBUS_I2C_BLOCK_DATA:
            print("\t\t Message size is I2C_SMBUS_I2C_BLOCK_DATA")

    # Reproduce a failing Quick write transaction
    if (
        command == I2C_SMBUS
        and msg.read_write == I2C_SMBUS_WRITE
        and msg.size == I2C_SMBUS_QUICK
    ):
        raise IOError("Mocking SMBus Quick failed")


##########################################################################


def test_initialize_bmm_not_conform(mocker):
    mocker.patch("smbus2.smbus2.os.open", mock_open)
    mocker.patch("smbus2.smbus2.os.close", mock_close)
    mocker.patch("smbus2.smbus2.ioctl", mock_ioctl)

    bmm = BMM150(auto_init=False)

    with pytest.raises(ChipIdNotConformError):
        bmm.initialize()


def test_initialize_bmm_conform(mocker):
    global test_buffer

    test_buffer[0x40] = 0x32

    mocker.patch("smbus2.smbus2.os.open", mock_open)
    mocker.patch("smbus2.smbus2.os.close", mock_close)
    mocker.patch("smbus2.smbus2.ioctl", mock_ioctl)

    BMM150()


def test_read_bmm_raw(mocker):
    mocker.patch("smbus2.smbus2.os.open", mock_open)
    mocker.patch("smbus2.smbus2.os.close", mock_close)
    mocker.patch("smbus2.smbus2.ioctl", mock_ioctl)

    bmm = BMM150()

    assert bmm.read_raw_mag_data() == (2152, 2216, 9123, 4690)


def test_read_bmm_compensated(mocker):
    mocker.patch("smbus2.smbus2.os.open", mock_open)
    mocker.patch("smbus2.smbus2.os.close", mock_close)
    mocker.patch("smbus2.smbus2.ioctl", mock_ioctl)

    bmm = BMM150()
    print(bmm.read_mag_data())
    assert bmm.read_mag_data() == (
        1124.385851468458,
        1161.2109437176882,
        -1146.9572546654647,
    )
